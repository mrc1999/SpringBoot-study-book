package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void contextLoads() {
		
		//userRepository.insertData();
		//userRepository.listData();
		
		//userRepository.batchInsert();
		
		userRepository.batchInsert2();
	}

}
