package com.example.demo.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;

@Repository
public class UserRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private SimpleJdbcInsert simpleJdbcInstall;
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	/***
	 * 插入数据
	 * 
	 * @Author MRC
	 * @Date 2019年6月4日 上午11:12:56
	 */
	public void insertData() {
		
		Arrays.asList("123","456").forEach(bar -> {
			
			jdbcTemplate.update("insert into user(id,username,password) values(?,?,?)", bar,bar,bar);
			
		});
		
		
		HashMap<String, String> row = new HashMap<>();
		row.put("username", "ddd");
		row.put("password", "345");
		
		
		Number id = simpleJdbcInstall.executeAndReturnKey(row);
		System.out.println(id);
		
	}
	
	public void listData() {
		//查询单个
		System.out.println(jdbcTemplate.queryForObject("select count(id) from user", Long.class));
		
		//查询list
		List<String> list = jdbcTemplate.queryForList("select id from user",String.class);
		
		list.forEach(id -> {
			
			System.out.println("id--->"+id);
			
		});
		
		
		//查询出来的数据与对象进行关联
		List<User> users = jdbcTemplate.query("select * from user", new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet arg0, int arg1) throws SQLException {
				
				return new User(arg0.getString("id"), arg0.getString("username"), arg0.getString("password"));
			}
		});
		
		
		users.forEach(user -> {
			System.out.println("user--->"+user);
			
		});
	}
	
	@Bean
	@Autowired
	public SimpleJdbcInsert simpleJdbcInsert(JdbcTemplate jdbcTemplate) {
		
		//将User表与SimpleJdbcInsert关联到一起
		return new SimpleJdbcInsert(jdbcTemplate).withTableName("user").usingGeneratedKeyColumns("id");
		
	}
	
	@Bean
	@Autowired
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}
	
	
	/**
	 * Sql批量操作
	 * 
	 * @Author MRC
	 * @Date 2019年6月4日 上午11:40:10
	 */
	public void batchInsert() {
		//使用Jdbc批量操作数据
		jdbcTemplate.batchUpdate("insert into user(username,password) values(?,?)", new BatchPreparedStatementSetter() {
			
			//sql语句里面的？进行一个赋值
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				
				ps.setString(1, "user-"+i);
				ps.setString(2, "pass-"+i);
				
			}
			//批量操作次数
			@Override
			public int getBatchSize() {
				return 5;
			}
		});
	}
	/***
	 * 使用namedParameterJdbcTemplate批量操作数据
	 * 
	 * @Author MRC
	 * @Date 2019年6月4日 上午11:43:01
	 */
	public void batchInsert2() {
		
		List<User> users = new ArrayList<>();
		
		users.add(new User("", "user-11", "pass-11"));
		users.add(new User("", "user-12", "pass-12"));
		users.add(new User("", "user-13", "pass-13"));
		
		
		namedParameterJdbcTemplate.batchUpdate("insert into user(username,password) values(:username,:password)", SqlParameterSourceUtils.createBatch(users));
		
	}
	
}
